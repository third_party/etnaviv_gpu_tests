#ifndef H_CMDSTREAM_FUCHSIA
#define H_CMDSTREAM_FUCHSIA

#include <stdint.h>

struct etna_dev {
    uint32_t placeholder;
};

struct etna_cmd_stream {
    uint32_t placeholder;
};

struct etna_bo {
    uint32_t placeholder;
};

struct drm_test_info {
    struct etna_dev* dev;
    struct etna_cmd_stream* stream;
};

enum {
    ETNA_RELOC_WRITE = 1,
    ETNA_RELOC_READ = 2,
};

enum {
    DRM_ETNA_GEM_CACHE_UNCACHED = 1,
};

void etna_set_state(struct etna_cmd_stream *stream, uint32_t address, uint32_t value);
void etna_set_state_from_bo(struct etna_cmd_stream *stream,
        uint32_t address, struct etna_bo *bo, uint32_t reloc_flags);
void etna_stall(struct etna_cmd_stream *stream, uint32_t from, uint32_t to);

struct etna_bo* etna_bo_new(void* dev, uint32_t size, uint32_t flags);
void* etna_bo_map(struct etna_bo* bo);

void etna_cmd_stream_finish(struct etna_cmd_stream* stream);

struct drm_test_info* drm_test_setup(int argc, char** argv);
void drm_test_teardown(struct drm_test_info* info);

#endif // H_CMDSTREAM_FUCHSIA
